FROM node
WORKDIR /app
COPY src .
RUN npm install
ENTRYPOINT npm start