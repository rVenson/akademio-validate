const express = require('express');
const fs = require('fs');
const path = require('path');

const app = express();
const port = process.env.PORT || 3000;
const logFilePath = path.join(__dirname, 'requests.log');

// Middleware to parse JSON bodies
app.use(express.json());

// Route to handle incoming JSON data
app.post('/', (req, res) => {
    const requestData = req.body;

    // Append the JSON data to the log file
    fs.appendFile(logFilePath, JSON.stringify(requestData) + '\n', (err) => {
        if (err) {
            console.error('Error writing to log file', err);
            return res.status(500).send('Internal Server Error');
        }

        res.status(200).send('Data received and logged');
    });
});

// Healthcheck route
app.get('/', async (_req, res, _next) => {
    const healthcheck = {
        uptime: process.uptime(),
        message: 'OK',
        timestamp: Date.now()
    };
    try {
        res.send(healthcheck);
    } catch (error) {
        healthcheck.message = error;
        res.status(503).send();
    }
});

// Start the server
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
